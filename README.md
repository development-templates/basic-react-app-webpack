# basic-react-app-webpack

Basic React App setup with Webpack Dev Server

## Install Dependencies

You can install all the required packages using the following command.

```sh
npm ci
```

- It will delete your node_modules folder to ensure a clean state.
- It will look in your `package-lock.json` to install all the dependencies with the exact version.
- Unlike `npm install`, `npm ci` will never modify your package-lock.json. It does however expect a `package-lock.json` file in your project — if you do not have this file, `npm ci` will not work and you have to use `npm install` instead.

If you use `npm ci`, you’ll get reliable builds. This is useful when you’re running in a continuous integration tool like Jenkins or GitLab CI.

## Start the App

You can start the app using the following command

```sh
npm start
```
