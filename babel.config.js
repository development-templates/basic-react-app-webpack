'use strict'

module.exports = {
    sourceType: 'unambiguous',
    plugins: [
        '@babel/plugin-transform-runtime'
    ],
    presets: [
        '@babel/react',
        '@babel/env'
    ]
}