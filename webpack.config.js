const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');

process.env.NODE_ENV = "production";

module.exports = {
    mode: "production",
    target: "web",
    devtool: "source-map",
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, "build"),
        publicPath: "/",
        filename: 'build.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/public/index.html',
            favicon: "src/public/favicon.ico"
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-react', '@babel/preset-env']
                        }
                    }
                ] 
            },
            {
                test: /(\.css)$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    }
}
